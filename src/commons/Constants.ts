export class Constants {

    public static TITLE = 'Contoso University';

    public static CLIENT_ANGULAR = 'client_angular';

    public static AUTH_SCOPE = 'openid profile ContosoUniversity.API';

    public static URL_SERVER = 'https://localhost:44313';
    public static URL_API = 'https://localhost:44350';
    // public static URL_SERVER = 'https://contosouniversityis.azurewebsites.net';
    // public static URL_API = 'https://contosouniversityapi.azurewebsites.net';
    
    public static API_STUDENTS = 'Students';
    public static API_INSTRUCTORS = 'Instructors';
    public static API_DEPARTMENTS = 'Department';
    public static API_COURSES = 'Courses';
    
}