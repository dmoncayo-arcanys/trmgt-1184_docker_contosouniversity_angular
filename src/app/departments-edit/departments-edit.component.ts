import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Department } from '../../models/Department'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-departments-edit',
  templateUrl: './departments-edit.component.html',
  styleUrls: ['./departments-edit.component.css']
})
export class DepartmentsEditComponent implements OnInit {

  department: Department = new Department();
  error: string = '';
  subscription: any;
  instructorList: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Department - ' + Constants.TITLE);
    this.getDepartment();
    this.getInstructors();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getInstructors() {
    this.instructorList = [];
    this.apiService.get(Constants.API_INSTRUCTORS).subscribe((data: any) => {
      if(data.status === 1) {
        this.instructorList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

  getDepartment() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_DEPARTMENTS, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.department.departmentID = data.response.departmentID;
          this.department.name = data.response.name;
          this.department.budget = data.response.budget;
          this.department.instructorID = data.response.instructorID;
        } else {
          this.error = data.message;
        }
      });
    });
  }

  updateDepartment() {
    if(this.department.validate()) {
      this.apiService.update(
        Constants.API_DEPARTMENTS, 
        this.department.departmentID.toString(), 
        this.department.get()).subscribe((data: any) => {
          this.router.navigate(['departments']);
        });
    } else {
      this.error = 'Please complete all the fields before saving.';
    }
  }

}
