import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  studentList: any;
  error: string = '';
  spinner: boolean = true;

  constructor(private apiService: ApiService, private title: Title) { }

  ngOnInit(): void {
    this.title.setTitle('Students - ' + Constants.TITLE);
    this.getStudents();
  }

  getStudents() {
    this.studentList = [];
    this.apiService.get(Constants.API_STUDENTS).subscribe((data: any) => {
      this.spinner = false;
      if(data.status === 1) {
        this.studentList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

}
