import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Instructor } from '../../models/Instructor'
import { Constants } from '../../commons/Constants'
import { Utils } from '../../commons/Utils'

@Component({
  selector: 'app-instructors-edit',
  templateUrl: './instructors-edit.component.html',
  styleUrls: ['./instructors-edit.component.css']
})
export class InstructorsEditComponent implements OnInit {

  instructor: Instructor = new Instructor();
  error: string = '';
  subscription: any;
  instructorList: any;
  courseAssignments: any;
  courseList: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Instructor - ' + Constants.TITLE);
    this.getInstructor();
    this.getCourses();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getCourses() {
    this.courseList = [];
    this.apiService.get(Constants.API_COURSES).subscribe((data: any) => {
      if(data.status === 1) {
        this.courseList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

  getInstructor() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_INSTRUCTORS, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.instructor.id = data.response.id;
          this.instructor.lastName = data.response.lastName;
          this.instructor.firstMidName = data.response.firstMidName;
          this.instructor.hireDate = Utils.formatDate(new Date(data.response.hireDate));
          this.courseAssignments = [];
          for(let index = 0; index < data.response.courseAssignments.length; index++) {
            this.courseAssignments.push(data.response.courseAssignments[index].courseID);
          }
        } else {
          this.error = data.message;
        }
      });
    });
  }

  updateInstructor() {
    if(this.instructor.validate()) {
      this.apiService.update(
        Constants.API_INSTRUCTORS, 
        this.instructor.id.toString(), 
        {
          instructors: this.instructor.get(),
          selectedCourses: this.courseAssignments,
        }).subscribe((data: any) => {
          this.router.navigate(['instructors']);
        });
    } else {
      this.error = 'Please complete all the fields before saving.';
    }
  }

}
