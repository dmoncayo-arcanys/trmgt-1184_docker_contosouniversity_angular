import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsDeleteComponent } from './instructors-delete.component';

describe('InstructorsDeleteComponent', () => {
  let component: InstructorsDeleteComponent;
  let fixture: ComponentFixture<InstructorsDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstructorsDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
