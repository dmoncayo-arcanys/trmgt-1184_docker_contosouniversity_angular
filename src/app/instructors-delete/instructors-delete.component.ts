import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Instructor } from '../../models/Instructor'
import { Constants } from '../../commons/Constants'
import { Utils } from '../../commons/Utils'

@Component({
  selector: 'app-instructors-delete',
  templateUrl: './instructors-delete.component.html',
  styleUrls: ['./instructors-delete.component.css']
})
export class InstructorsDeleteComponent implements OnInit {

  instructor: Instructor = new Instructor();
  error: string = '';
  subscription: any;
  instructorList: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Delete Instructor - ' + Constants.TITLE);
    this.getInstructor();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getInstructor() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_INSTRUCTORS, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.instructor.id = data.response.id;
          this.instructor.lastName = data.response.lastName;
          this.instructor.firstMidName = data.response.firstMidName;
          this.instructor.hireDate = Utils.formatDate(new Date(data.response.hireDate));
        } else {
          this.error = data.message;
        }
      });
    });
  }

  deleteInstructor() {
    if(this.instructor.validate()) {
      this.apiService.delete(
        Constants.API_INSTRUCTORS, 
        this.instructor.id.toString()).subscribe((data: any) => {
          this.router.navigate(['instructors']);
        });
    } else {
      this.error = 'Please complete all the fields before saving.';
    }
  }

}
