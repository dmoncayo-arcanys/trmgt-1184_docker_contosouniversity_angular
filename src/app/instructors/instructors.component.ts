import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-instructors',
  templateUrl: './instructors.component.html',
  styleUrls: ['./instructors.component.css']
})
export class InstructorsComponent implements OnInit {

  instructorList: any;
  error: string = '';
  spinner: boolean = true;

  constructor(private apiService: ApiService, private title: Title) { }

  ngOnInit(): void {
    this.title.setTitle('Instructors - ' + Constants.TITLE);
    this.getInstructors();
  }

  getInstructors() {
    this.instructorList = [];
    this.apiService.get(Constants.API_INSTRUCTORS).subscribe((data: any) => {
      this.spinner = false;
      if(data.status === 1) {
        this.instructorList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

}
