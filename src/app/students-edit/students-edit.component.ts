import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Student } from '../../models/Student'
import { Constants } from '../../commons/Constants'
import { Utils } from '../../commons/Utils'

@Component({
  selector: 'app-students-edit',
  templateUrl: './students-edit.component.html',
  styleUrls: ['./students-edit.component.css']
})
export class StudentsEditComponent implements OnInit {
  
  student: Student = new Student();
  error: string = '';
  subscription: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Student - ' + Constants.TITLE);
    this.getStudent();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getStudent() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_STUDENTS, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.student.id = data.response.id;
          this.student.lastName = data.response.lastName;
          this.student.firstMidName = data.response.firstMidName;
          this.student.enrollmentDate = Utils.formatDate(new Date(data.response.enrollmentDate));
        } else {
          this.error = data.message;
        }
      });
    });
  }

  updateStudent() {
    if(this.student.validate()) {
      this.apiService.update(
        Constants.API_STUDENTS, 
        this.student.id.toString(), 
        this.student.get()).subscribe((data: any) => {
          this.router.navigate(['students']);
        });
    } else {
      this.error = 'Please complete all the fields before saving.';
    }
  }

}
