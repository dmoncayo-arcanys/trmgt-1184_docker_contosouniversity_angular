import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsDetailComponent } from './instructors-detail.component';

describe('InstructorsDetailComponent', () => {
  let component: InstructorsDetailComponent;
  let fixture: ComponentFixture<InstructorsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstructorsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
