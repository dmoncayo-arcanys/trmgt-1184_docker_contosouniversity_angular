import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Instructor } from '../../models/Instructor'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-instructors-detail',
  templateUrl: './instructors-detail.component.html',
  styleUrls: ['./instructors-detail.component.css']
})
export class InstructorsDetailComponent implements OnInit {

  instructor: Instructor = new Instructor();
  error: string = '';
  subscription: any;
  instructorList: any;
  courseList: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.title.setTitle('Detail Instructor - ' + Constants.TITLE);
    this.getInstructor();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getInstructor() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_INSTRUCTORS, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.instructor = data.response;
          this.courseList = data.response.courseAssignments;
          console.log("data ", this.courseList);
        } else {
          this.error = data.message;
        }
      });
    });
  }

}
