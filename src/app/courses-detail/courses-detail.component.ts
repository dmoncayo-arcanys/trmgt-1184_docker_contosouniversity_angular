import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Course } from '../../models/Course'
import { Department } from '../../models/Department'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-courses-detail',
  templateUrl: './courses-detail.component.html',
  styleUrls: ['./courses-detail.component.css']
})
export class CoursesDetailComponent implements OnInit {

  course: Course = new Course();
  department: Department = new Department();
  error: string = '';
  subscription: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.title.setTitle('Detail Course - ' + Constants.TITLE);
    this.getCourse();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getCourse() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_COURSES, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.course = data.response;
          this.department = data.response.department;
        } else {
          this.error = data.message;
        }
      });
    });
  }

}
