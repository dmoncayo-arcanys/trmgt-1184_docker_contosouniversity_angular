import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Student } from '../../models/Student'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-students-create',
  templateUrl: './students-create.component.html',
  styleUrls: ['./students-create.component.css']
})
export class StudentsCreateComponent implements OnInit {

  student: Student = new Student();
  error: string = '';
  subscription: any;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Create Student - ' + Constants.TITLE);
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  createStudent() {
    if(this.student.validate()) {
      this.apiService.create(
        Constants.API_STUDENTS, 
        this.student.get()).subscribe((data: any) => {
          this.router.navigate(['students']);
        });
    } else {
      this.error = 'Please complete all the fields before saving.';
    }
  }

}
