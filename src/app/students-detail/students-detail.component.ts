import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Student } from '../../models/Student'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-students-detail',
  templateUrl: './students-detail.component.html',
  styleUrls: ['./students-detail.component.css']
})
export class StudentsDetailComponent implements OnInit {

  student: Student = new Student();
  error: string = '';
  subscription: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.title.setTitle('Detail Student - ' + Constants.TITLE);
    this.getStudent();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getStudent() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_STUDENTS, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.student = data.response;
        } else {
          this.error = data.message;
        }
      });
    });
  }

}
