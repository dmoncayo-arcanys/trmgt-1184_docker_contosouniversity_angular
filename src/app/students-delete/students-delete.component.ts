import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Student } from '../../models/Student'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-students-delete',
  templateUrl: './students-delete.component.html',
  styleUrls: ['./students-delete.component.css']
})
export class StudentsDeleteComponent implements OnInit {

  student: Student = new Student();
  error: string = '';
  subscription: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Delete Student - ' + Constants.TITLE);
    this.getStudent();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getStudent() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_STUDENTS, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.student = data.response;
        } else {
          this.error = data.message;
        }
      });
    });
  }

  deleteStudent() {
    this.apiService.delete(
      Constants.API_STUDENTS, 
      this.student.id.toString()).subscribe((data: any) => {
        this.router.navigate(['students']);
      });
  }

}
