import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  
  courseList: any;
  error: string = '';
  spinner: boolean = true;

  constructor(private apiService: ApiService, private title: Title) { }

  ngOnInit(): void {
    this.title.setTitle('Courses - ' + Constants.TITLE);
    this.getCourses();
  }

  getCourses() {
    this.courseList = [];
    this.apiService.get(Constants.API_COURSES).subscribe((data: any) => {
      this.spinner = false;
      if(data.status === 1) {
        this.courseList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

}
