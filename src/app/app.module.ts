import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AuthModule, LogLevel } from 'angular-auth-oidc-client';
import { Constants } from '../commons/Constants'

import { ApiService } from '../services/ApiService'
import { AuthService } from '../services/AuthService'

import { AppComponent } from './app.component';
import { CoursesComponent } from './courses/courses.component';
import { DepartmentsComponent } from './departments/departments.component';
import { HomeComponent } from './home/home.component';
import { InstructorsComponent } from './instructors/instructors.component';
import { StudentsComponent } from './students/students.component';
import { StudentsEditComponent } from './students-edit/students-edit.component';
import { StudentsDetailComponent } from './students-detail/students-detail.component';
import { StudentsDeleteComponent } from './students-delete/students-delete.component';
import { StudentsCreateComponent } from './students-create/students-create.component';
import { CoursesCreateComponent } from './courses-create/courses-create.component';
import { CoursesDeleteComponent } from './courses-delete/courses-delete.component';
import { CoursesDetailComponent } from './courses-detail/courses-detail.component';
import { CoursesEditComponent } from './courses-edit/courses-edit.component';
import { DepartmentsCreateComponent } from './departments-create/departments-create.component';
import { DepartmentsEditComponent } from './departments-edit/departments-edit.component';
import { DepartmentsDeleteComponent } from './departments-delete/departments-delete.component';
import { DepartmentsDetailComponent } from './departments-detail/departments-detail.component';
import { InstructorsCreateComponent } from './instructors-create/instructors-create.component';
import { InstructorsDeleteComponent } from './instructors-delete/instructors-delete.component';
import { InstructorsDetailComponent } from './instructors-detail/instructors-detail.component';
import { InstructorsEditComponent } from './instructors-edit/instructors-edit.component';
import { AlertComponent } from '../components/alert/alert.component';
import { SpinnerComponent } from '../components/spinner/spinner.component';

const routes: Routes = [
  { path: '', component: HomeComponent, },
  { path: 'students', component: StudentsComponent, },
  { path: 'student/edit/:id', component: StudentsEditComponent, },
  { path: 'student/detail/:id', component: StudentsDetailComponent, },
  { path: 'student/delete/:id', component: StudentsDeleteComponent, },
  { path: 'student/create', component: StudentsCreateComponent, },
  { path: 'departments', component: DepartmentsComponent, },
  { path: 'department/edit/:id', component: DepartmentsEditComponent, },
  { path: 'department/detail/:id', component: DepartmentsDetailComponent, },
  { path: 'department/delete/:id', component: DepartmentsDeleteComponent, },
  { path: 'department/create', component: DepartmentsCreateComponent, },
  { path: 'instructors', component: InstructorsComponent, },
  { path: 'instructor/edit/:id', component: InstructorsEditComponent, },
  { path: 'instructor/detail/:id', component: InstructorsDetailComponent, },
  { path: 'instructor/delete/:id', component: InstructorsDeleteComponent, },
  { path: 'instructor/create', component: InstructorsCreateComponent, },
  { path: 'courses', component: CoursesComponent, },
  { path: 'course/edit/:id', component: CoursesEditComponent, },
  { path: 'course/detail/:id', component: CoursesDetailComponent, },
  { path: 'course/delete/:id', component: CoursesDeleteComponent, },
  { path: 'course/create', component: CoursesCreateComponent, },
  { path: '**', component: HomeComponent, },
];

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    DepartmentsComponent,
    HomeComponent,
    InstructorsComponent,
    StudentsComponent,
    StudentsEditComponent,
    StudentsDetailComponent,
    StudentsDeleteComponent,
    StudentsCreateComponent,
    CoursesCreateComponent,
    CoursesDeleteComponent,
    CoursesDetailComponent,
    CoursesEditComponent,
    DepartmentsCreateComponent,
    DepartmentsEditComponent,
    DepartmentsDeleteComponent,
    DepartmentsDetailComponent,
    InstructorsCreateComponent,
    InstructorsDeleteComponent,
    InstructorsDetailComponent,
    InstructorsEditComponent,
    AlertComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    AuthModule.forRoot({
      config: {
        authority: Constants.URL_SERVER,
        redirectUrl: window.location.origin,
        postLogoutRedirectUri: window.location.origin,
        clientId: Constants.CLIENT_ANGULAR,
        scope: Constants.AUTH_SCOPE,
        responseType: 'code',
        silentRenew: true,
        useRefreshToken: true,
        logLevel: LogLevel.Debug,
      },
    }),
  ],
  providers: [
    ApiService,
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
