import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsCreateComponent } from './instructors-create.component';

describe('InstructorsCreateComponent', () => {
  let component: InstructorsCreateComponent;
  let fixture: ComponentFixture<InstructorsCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstructorsCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
