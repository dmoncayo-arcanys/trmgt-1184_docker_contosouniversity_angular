import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Instructor } from '../../models/Instructor'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-instructors-create',
  templateUrl: './instructors-create.component.html',
  styleUrls: ['./instructors-create.component.css']
})
export class InstructorsCreateComponent implements OnInit {

  instructor: Instructor = new Instructor();
  error: string = '';
  subscription: any;
  instructorList: any;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Create Instructor - ' + Constants.TITLE);
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  createInstructor() {
    if(this.instructor.validate()) {
      this.apiService.create(
        Constants.API_INSTRUCTORS, 
        this.instructor.get()).subscribe((data: any) => {
          this.router.navigate(['instructors']);
        });
    } else {
      this.error = 'Please complete all the fields before saving.';
    }
  }

}
