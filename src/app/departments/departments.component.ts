import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  
  departmentList: any;
  error: string = '';
  spinner: boolean = true;

  constructor(private apiService: ApiService, private title: Title) { }

  ngOnInit(): void {
    this.title.setTitle('Departments - ' + Constants.TITLE);
    this.getDepartments();
  }

  getDepartments() {
    this.departmentList = [];
    this.apiService.get(Constants.API_DEPARTMENTS).subscribe((data: any) => {
      this.spinner = false;
      if(data.status === 1) {
        this.departmentList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

}
