import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  subscription: any;
  title: string = 'Welcome to Contoso University';
  description: string = 'Testing React, .NET Core Web API, and IdentityServer4';
  spinner: boolean = false;

  constructor(private route: ActivatedRoute, private titles: Title) { }

  ngOnInit(): void {
    this.titles.setTitle(Constants.TITLE);
    this.subscription = this.route.queryParams.subscribe(params => {
      let code = params.code;
      if(code !== null && code !== undefined && code !== '') {
        this.title = 'Authenticating Session ...';
        this.description = 'Please wait while we are authenticating this session';
        this.spinner = true;
      } else {
        this.title = 'Welcome to Contoso University';
        this.description = 'Testing React, .NET Core Web API, and IdentityServer4';
        this.spinner = false;
      }
    });
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

}
