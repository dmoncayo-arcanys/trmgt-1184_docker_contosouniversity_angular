import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Department } from '../../models/Department'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-departments-create',
  templateUrl: './departments-create.component.html',
  styleUrls: ['./departments-create.component.css']
})
export class DepartmentsCreateComponent implements OnInit {

  department: Department = new Department();
  error: string = '';
  subscription: any;
  instructorList: any;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Create Department - ' + Constants.TITLE);
    this.getInstructors();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getInstructors() {
    this.instructorList = [];
    this.apiService.get(Constants.API_INSTRUCTORS).subscribe((data: any) => {
      if(data.status === 1) {
        this.instructorList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

  createDepartment() {
    if(this.department.validate()) {
      this.apiService.create(
        Constants.API_DEPARTMENTS, 
        this.department.get()).subscribe((data: any) => {
          this.router.navigate(['departments']);
        });
    } else {
      this.error = 'Please complete all the fields before saving.';
    }
  }

}
