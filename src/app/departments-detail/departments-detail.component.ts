import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Department } from '../../models/Department'
import { Instructor } from '../../models/Instructor'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-departments-detail',
  templateUrl: './departments-detail.component.html',
  styleUrls: ['./departments-detail.component.css']
})
export class DepartmentsDetailComponent implements OnInit {

  department: Department = new Department();
  instructor: Instructor = new Instructor();
  error: string = '';
  subscription: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Detail Department - ' + Constants.TITLE);
    this.getDepartment();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getDepartment() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_DEPARTMENTS, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.department = data.response;
          this.instructor = data.response.administrator;
        } else {
          this.error = data.message;
        }
      });
    });
  }

}
