import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Course } from '../../models/Course'
import { Department } from '../../models/Department'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-courses-delete',
  templateUrl: './courses-delete.component.html',
  styleUrls: ['./courses-delete.component.css']
})
export class CoursesDeleteComponent implements OnInit {

  course: Course = new Course();
  department: Department = new Department();
  error: string = '';
  subscription: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Delete Course - ' + Constants.TITLE);
    this.getCourse();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getCourse() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_COURSES, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.course = data.response;
          this.department = data.response.department;
        } else {
          this.error = data.message;
        }
      });
    });
  }
  
  deleteCourse() {
    this.apiService.delete(
      Constants.API_COURSES, 
      this.course.courseID.toString()).subscribe((data: any) => {
        this.router.navigate(['courses']);
      });
  }

}
