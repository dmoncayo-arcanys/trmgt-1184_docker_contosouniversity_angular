import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Course } from '../../models/Course'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-courses-create',
  templateUrl: './courses-create.component.html',
  styleUrls: ['./courses-create.component.css']
})
export class CoursesCreateComponent implements OnInit {

  course: Course = new Course();
  error: string = '';
  subscription: any;
  departmentList: any;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Create Course - ' + Constants.TITLE);
    this.getDepartments();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getDepartments() {
    this.departmentList = [];
    this.apiService.get(Constants.API_DEPARTMENTS).subscribe((data: any) => {
      if(data.status === 1) {
        this.departmentList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

  createCourse() {
    if(this.course.validate()) {
      this.apiService.create(
        Constants.API_COURSES, 
        this.course.get()).subscribe((data: any) => {
          this.router.navigate(['courses']);
        });
    } else {
      this.error = 'Please complete all the fields before saving. Credits are from 0 to 5 only.';
    }
  }

}
