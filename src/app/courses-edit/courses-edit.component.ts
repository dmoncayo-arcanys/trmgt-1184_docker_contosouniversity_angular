import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";
import { ApiService } from '../../services/ApiService';
import { Course } from '../../models/Course'
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-courses-edit',
  templateUrl: './courses-edit.component.html',
  styleUrls: ['./courses-edit.component.css']
})
export class CoursesEditComponent implements OnInit {

  course: Course = new Course();
  error: string = '';
  subscription: any;
  departmentList: any;
  spinner: boolean = true;

  constructor(
    private apiService: ApiService, 
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.title.setTitle('Edit Course - ' + Constants.TITLE);
    this.getCourse();
    this.getDepartments();
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  getCourse() {
    this.subscription = this.route.params.subscribe(params => {
      let id = params['id'];
      this.apiService.getById(Constants.API_COURSES, id).subscribe((data: any) => {
        this.spinner = false;
        if(data.status === 1) {
          this.course.courseID = data.response.courseID;
          this.course.title = data.response.title;
          this.course.credits = data.response.credits;
          this.course.departmentID = data.response.departmentID;
        } else {
          this.error = data.message;
        }
      });
    });
  }

  getDepartments() {
    this.departmentList = [];
    this.apiService.get(Constants.API_DEPARTMENTS).subscribe((data: any) => {
      if(data.status === 1) {
        this.departmentList = data.response;
      } else {
        this.error = data.message;
      }
    });
  }

  updateCourse() {
    if(this.course.validate()) {
      this.apiService.update(
        Constants.API_COURSES, 
        this.course.courseID.toString(), 
        this.course.get()).subscribe((data: any) => {
          this.router.navigate(['courses']);
        });
    } else {
      this.error = 'Please complete all the fields before saving. Credits are from 0 to 5 only.';
    }
  }

}
