import { Component } from '@angular/core';
import { AuthService } from '../services/AuthService'
import {
  Router,
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  title = 'contoso-university';
  userEmail = '';
  isLoggedIn: boolean = false;
  loading: boolean = true;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    let _this = this;
    this.router.events.subscribe((e : RouterEvent) => {
      this.navigationInterceptor(e);
    })
    this.authService.checkAuth().subscribe(({ isAuthenticated, userData, accessToken, idToken }) => {
      this.isLoggedIn = isAuthenticated;
      this.userEmail = userData.name;
      console.log('isAuthenticated ', isAuthenticated);
      console.log('Authentication ', userData);
      console.log('accessToken ', accessToken);
      console.log('idToken ', idToken);
    });
    setInterval(function(){
      let token = _this.authService.get().getAccessToken();
      _this.isLoggedIn = token !== null;
    }, 1000);
  }

  login() {
    this.authService.login();
  }

  logout() {
    this.authService.logout();
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading = true
    }
    if (event instanceof NavigationEnd) {
      this.loading = false
    }
    if (event instanceof NavigationCancel) {
      this.loading = false
    }
    if (event instanceof NavigationError) {
      this.loading = false
    }
  }

}
