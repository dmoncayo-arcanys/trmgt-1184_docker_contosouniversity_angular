import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/AuthService'

@Component({
  selector: 'alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  
  @Input() error: string = '';
  isSession: boolean = false;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.isSession = this.error === 'Your session is expired. Please login.';
  }

  login() {
    this.authService.login();
  }

}
