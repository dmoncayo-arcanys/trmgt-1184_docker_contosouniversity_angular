export class Instructor {

    id: number;
    lastName: string;
    firstMidName: string;
    fullName: string;
    hireDate: string;

    constructor() { 
        this.id = 0;
        this.lastName = '';
        this.firstMidName = '';
        this.fullName = '';
        this.hireDate = '';
    }

    validate() {
        let flag = true;
        if(this.lastName.trim() === '') flag = false;
        if(this.firstMidName.trim() === '') flag = false;
        if(this.hireDate.trim() === '') flag = false;
        return flag;
    }

    get() {
        return {
            id: this.id,
            lastName: this.lastName,
            firstMidName: this.firstMidName,
            hireDate: this.hireDate,
        };
    }

}