export class Student {

    id: number;
    lastName: string;
    firstMidName: string;
    enrollmentDate: string;

    constructor() { 
        this.id = 0;
        this.lastName = '';
        this.firstMidName = '';
        this.enrollmentDate = '';
    }

    validate() {
        let flag = true;
        if(this.lastName.trim() === '') flag = false;
        if(this.firstMidName.trim() === '') flag = false;
        if(this.enrollmentDate.trim() === '') flag = false;
        return flag;
    }

    get() {
        return {
            id: this.id,
            lastName: this.lastName,
            firstMidName: this.firstMidName,
            enrollmentDate: this.enrollmentDate,
        };
    }
}