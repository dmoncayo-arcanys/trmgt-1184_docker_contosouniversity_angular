export class Department {

    departmentID: number;
    name: string;
    budget: number;
    startDate: any;
    instructorID: number;

    constructor() { 
        this.departmentID = 0;
        this.name = '';
        this.budget = 0;
        this.startDate = new Date();
        this.instructorID = 0;
    }

    validate() {
        let flag = true;
        if(this.name.trim() === '') flag = false;
        if(this.instructorID <= 0) flag = false;
        return flag;
    }

    get() {
        return {
            departmentID: this.departmentID,
            name: this.name,
            budget: this.budget,
            startDate: this.startDate,
            instructorID: this.instructorID,
        };
    }

}