export class Course {

    courseID: number;
    title: string;
    credits: number;
    departmentID: number;

    constructor() { 
        this.courseID = 0;
        this.title = '';
        this.credits = 0;
        this.departmentID = 0;
    }

    validate() {
        let flag = true;
        if(this.departmentID <= 0) flag = false;
        if(this.courseID <= 0) flag = false;
        if(this.title.trim() === '') flag = false;
        if(this.credits < 0 || this.credits >= 5) flag = false;
        return flag;
    }

    get() {
        return {
            courseID: this.courseID,
            title: this.title,
            credits: this.credits,
            departmentID: this.departmentID,
        };
    }

}