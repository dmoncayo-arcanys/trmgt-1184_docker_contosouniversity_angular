import { Injectable } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../commons/Constants'
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable()
export class ApiService {

    constructor(private oidcSecurityService: OidcSecurityService, private http: HttpClient) { }

    private header(): object {
        let token = this.oidcSecurityService.getAccessToken();
        return {
            headers: new HttpHeaders({
                Authorization: 'Bearer ' + token,
            }),
            responseType: 'json'
        }
    }

    private handleError(error: any) {
        let message = '';
        switch(error.status) {
            case 401:
                message = 'Your session is expired. Please login.';
                break;
            default:
                message = 'Cannot connect to the database. Please check your connection or try again later.';
                break;
        }
        return of({
            status: error.status,
            message: message,
        });
    }

    public get(url: string): Observable<any> {
        return this.http
            .get<any[]>(Constants.URL_API + '/' + url, this.header())
            .pipe(catchError(error => this.handleError(error)));
    }

    public getById(url: string, id: string): Observable<any> {
        return this.http
            .get<any[]>(Constants.URL_API + '/' + url + '/' + id, this.header())
            .pipe(catchError(error => this.handleError(error)));
    }

    public create(url: string, data: any): Observable<any> {
        return this.http
            .post<any[]>(Constants.URL_API + '/' + url, data, this.header())
            .pipe(catchError(error => this.handleError(error)));
    }

    public update(url: string, id: string, data: any): Observable<any> {
        return this.http
            .put<any[]>(Constants.URL_API + '/' + url + '/' + id, data, this.header())
            .pipe(catchError(error => this.handleError(error)));
    }

    public delete(url: string, id: string): Observable<any> {
        return this.http
            .delete<any[]>(Constants.URL_API + '/' + url + '/' + id, this.header())
            .pipe(catchError(error => this.handleError(error)));
    }

}