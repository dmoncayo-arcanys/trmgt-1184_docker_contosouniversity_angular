import { Injectable } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';

@Injectable()
export class AuthService {

    constructor(private oidcSecurityService: OidcSecurityService) { }

    get() {
        return this.oidcSecurityService;
    }

    checkAuth() {
        return this.oidcSecurityService.checkAuth();
    }

    login() {
        this.oidcSecurityService.authorize();
    }
    
    logout() {
        this.oidcSecurityService.logoff();
    }

}